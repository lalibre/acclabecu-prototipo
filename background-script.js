/*
Log that we received the message.
Then display a notification. The notification contains the qualification,
which we read from the message.
*/
var iconUrl;
var iconPath;

function notify(message) {
  console.log("background script received message");
  var title = "Advertencia Oferta Laboral";
  //console.log(message.result);

  switch (message.result) {
    case 'A':
  		iconUrl = browser.extension.getURL("icons/A.png");
      iconPath =  "icons/A.png"
      break;
    
    case 'B+':
    	iconUrl = browser.extension.getURL("icons/B+.png");
      iconPath =  "icons/B+.png"
      break;

    case 'B':
    	iconUrl = browser.extension.getURL("icons/B.png");
      iconPath =  "icons/B.png"
      break;

    case 'C+':
    	iconUrl = browser.extension.getURL("icons/C+.png");
      iconPath = "icons/C+.png"
      break;

    case 'C':
    	iconUrl = browser.extension.getURL("icons/C.png");
      iconPath =  "icons/C.png"
      break;

    default:
    	iconUrl = browser.extension.getURL("icons/security-48.png");
      iconPath = "icons/security-48.png"
      break;
  }

    /*Update icon*/
      browser.tabs.query({active:true, windowType:"normal", currentWindow: true},function(d){
        var tabId = d[0].id;
        browser.browserAction.setIcon({path: iconPath, tabId: tabId});
    })


  /*Crete notificaction*/
  browser.notifications.create({
    "type": "basic",
    "iconUrl": iconUrl,
    "title": title,
    "message": "La calificación de la Oferta Laboral es: " + message.result
  });
}

/*
Assign `notify()` as a listener to messages from the content script.
*/
browser.runtime.onMessage.addListener(notify);
