const HOST = {
  multitrabajo: "www.multitrabajos.com",
  computrabajo: "www.computrabajo.com.ec",
};

const RATE_STARS = [
  {
    max: 23,
    min: 0,
    rateTotal: 1
  },
  {
    max: 47,
    min: 24,
    rateTotal: 2
  },
  {
    max: 72,
    min: 48,
    rateTotal: 3
  },
  {
    max: 95,
    min: 73,
    rateTotal: 4
  },
  {
    max: 150,
    min: 96,
    rateTotal: 5
  }
];

const URL_MULTITRABAJO_API = 'https://www.multitrabajos.com/api/candidates/fichaAvisoNormalizada/';

const FIELDS = {
  salary: "SALARIO",
  date: "DATEPOSTED",
  enterprise: "EMPRESA",
  ratingValue: "RATINGVALUE",
  confidential: "CONFIDENCIAL",
  noSpecified: "NO ESPECIFICADO",
};
const RANGE_CP = [
  {
    max: 5,
    min: 4.1,
    rating: "A",
    background: "background-color: LimeGreen;"
  },
  {
    max: 4,
    min: 3.1,
    rating: "B+",
    background: "background-color: LimeGreen;"
  },
  {
    max: 3,
    min: 2.1,
    rating: "B",
    background: "background-color: DarkOrange;"
  },
  {
    max: 2,
    min: 1.1,
    rating: "C+",
    background: "background-color: DarkOrange;"
  },
  {
    max: 1,
    min: 0,
    rating: "C",
    background: "background-color: red;"
  },
];

const TAGS = {
  ul: "UL",
  meta: "META",
};

const RATING_3 = {
  MAX: 1,
  MIN: 0,
};

const RATING_CP = {
  MAX: 5,
  MIN: 0,
};

const STYLE_TOOLTIP_MULTITRABAJO = `
  .tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
  }
  
  .tooltip .tooltiptext {
    visibility: hidden;
    width: 200px;
    height: 120px;
    background-color: #fff;
    color: #555;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    
    /* Position the tooltip */
    position: absolute;
    z-index: 1;
    top: -5px;
    right: 105%;
  }
  
  .tooltip:hover .tooltiptext {
    visibility: visible;
  }
  `;

const STYLE_TOOLTIP_COMPUTRABAJO = `
  .tooltip {
    position: relative;
    border-bottom: 1px dotted black;
  }
  
  .tooltip .tooltiptext {
    visibility: hidden;
    width: 330px;
    height: 120px;
    background-color: #fff;
    color: #555;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    
    
    /* Position the tooltip */
    position: absolute;
    z-index: 1;
    top: -5px;
    right: 105%;
  }
  
  .tooltip:hover .tooltiptext {
    visibility: visible;
  }
  `;

const STYLE_TABLE = `
  table.timecard{
    margin: auto;
  }
  table.timecard td, table.timecard th {
  width: 25px;
  height: 25px;
  border: 1px solid #ccc;
  text-align: center;
  font-size: 11px;
  }
  table.timecard th {
  background: #20b2aa;
  border-color: white;
  color: white;
  }
  `;

(async function () {
  /**
   * Check and set a global guard variable.
   * If this content script is injected into the same page again,
   * it will do nothing next time.
   */
  if (window.hasRun) {
    return;
  }
  window.hasRun = true;

  var URLactual = window.location;

  let q= await fetchXmlMehod(URLactual.href);

  switch (URLactual.hostname) {
    case HOST.computrabajo:      
      let c = Computrabajo();
      addChildComputrabajo(c,q);
      browser.runtime.sendMessage({ "result": c.ratingComputrabajo });
      break;
    case HOST.multitrabajo:
      let idJob = (URLactual.pathname).split("-");
      idJob = (idJob[idJob.length - 1]).split(".");
      if(idJob[1].includes("html/")){
        let nJob=idJob[1].split("/");
        idJob=nJob[1];
      }else{
        idJob=idJob[0];
      }
      let m = await Multitrabajo(idJob);
      addChildMultitrabajo(m,q)
      browser.runtime.sendMessage({ "result": m.ratingRangeMultitrabajo });
      break;

    default:
      break;
  }

})();


/*
* Open new tab Multitrabajo
*/
document.addEventListener("click", (e) => {

    console.log("INGRESA")

    let target = e.target.classList;   
    let url;

    /*Url is extracted from href class "Card__CardContentWrapper-i6v2cb-1"*/  
    if (target.contains("Card__CardContentWrapper-i6v2cb-1")) { 
      url = e.target.href;
      openInNewTab(url);
    } 
    else{
      /*Url is extracted from parent from level 2 of classes "Card__CardHeader-i6v2cb-2 / Card__CardBody-i6v2cb-3 / Card__CardFooter-i6v2cb-4"*/ 
      if (target.contains("Card__CardHeader-i6v2cb-2") || target.contains("Card__CardBody-i6v2cb-3") || target.contains("Card__CardFooter-i6v2cb-4")){
      let parent = e.target.parentNode.href;
      url = parent;
      openInNewTab(url);   
      }
      else{
        if(target.contains("Card__Location-i6v2cb-9") || target.contains("Card__Title-i6v2cb-10") || target.contains("Card__ExtraInfo-i6v2cb-5"))
        {
          let parent2 = e.target.parentNode.parentNode.href;
          url = parent2;
          openInNewTab(url);
        }
        else{
          if(target.contains("Card__Workday-i6v2cb-12") || target.contains("Card__Published-i6v2cb-13"))
          {
            let parent3 = e.target.parentNode;
          }
        }
      }
    }
      
  e.preventDefault();
});


/*
*Open in new tab Job
*/
function openInNewTab(url) {
  const win = window.open(url, '_blank');
  win.focus();
}



/**
Add rating from Computrabajo
*/
function addChildComputrabajo(value,serviceReport) {
  let tablediv = tableInfoComputrabajo(value.enterpriseComputrabajo, value.salaryComputrabajo, value.ratingCP, value.verifiedEnterprise, value.compareDates,serviceReport);

  //add global style
  var style = document.createElement('style');
  style.innerHTML = STYLE_TOOLTIP_COMPUTRABAJO;
  document.head.appendChild(style);

  var style_table = document.createElement('style');
  style_table.innerHTML = STYLE_TABLE;
  document.head.appendChild(style_table);


  let view = document.getElementsByClassName("cm-8 box detalle_oferta box_image");
  var contentDiv = document.createElement("div");
  contentDiv.id = "rating";
  let colorBg = getBgColor(value.ratingComputrabajo);
  contentDiv.className = ('tooltip');
  contentDiv.innerText = value.ratingComputrabajo;

  contentDiv.style = "font-size:20px;padding-top:2%;position: absolute; left: 60%; top: 17%; border-radius: 5%; width:60px; height:80px; font-weight: bold; color:white; text-align: center;" + colorBg;


  let pValue = document.createElement("span");
  pValue.style = "position: absolute;margin-top: 40px;left: 25px;font-size: large;font-weight: bold;";
  pValue.className = ('tooltiptext');
  pValue.innerHTML = tablediv;

  contentDiv.appendChild(pValue);

  view[0].appendChild(contentDiv);

}

/**
Add rating from Multitrabajo
*/
function addChildMultitrabajo(value,serviceReport) {

  let tablediv = tableInfoMultitrabajo(value.enterpriseMultitrabajo, value.salaryMultitrabajo, value.compareDatesMultitrabajo,serviceReport);

  let view = document.getElementsByClassName("FichaAviso__FichaAvisoHeader-b2a7hd-2 cNGXuv");

  //add global style
  var style = document.createElement('style');
  style.innerHTML = STYLE_TOOLTIP_MULTITRABAJO;
  document.head.appendChild(style);

  var style_table = document.createElement('style');
  style_table.innerHTML = STYLE_TABLE;
  document.head.appendChild(style_table);

  //add conted div
  var contentDiv = document.createElement("div");
  contentDiv.id = "rating";
  let colorBg = getBgColor(value.ratingRangeMultitrabajo);
  contentDiv.style = "font-size:20px;padding-top:3%;border-radius: 5%; font-stretch: expanded; width:60px; font-weight: bold; color:white; text-align: center;" + colorBg;
  contentDiv.innerText = value.ratingRangeMultitrabajo;

  contentDiv.className = ('tooltip');

  //add txt to view in tooltip
  let pValue = document.createElement("span");
  pValue.style = "font-size: large;";
  pValue.className = ('tooltiptext');
  pValue.innerHTML = tablediv;

  //add to dom
  contentDiv.appendChild(pValue);

  view[0].appendChild(contentDiv);
}

//Generate Table for Tooltip
function tableInfoMultitrabajo(enterprise, salary, date, serviceReport) {

  let icon = browser.runtime.getURL("icons/security-48.png");
  let enterpriseSi = ""; let enterpriseNo = "";
  let salarySi = ""; let salaryNo = "";
  let dateSi = ""; let dateNo = "";
  let reportado = "";
  switch (serviceReport) {
    case 0:
      reportado = "No reportado"
      break;
    case 1:
      reportado = serviceReport+" Vez reportado"
      break;       
    default:
      reportado = serviceReport+" Veces reportado"
      break;
  };  

  if (enterprise !== 0)
    enterpriseSi = "x";
  else
    enterpriseNo = "x";

  if (salary !== 0)
    salarySi = "x";
  else
    salaryNo = "x";

  if (date !== 0)
    dateSi = "x";
  else
    dateNo = "x";

  let tablediv = `
  <table class="timecard">
    <tr>
    <th style="background-color:#fff"><img style="width:20px;" alt="" src="`+icon+`" /></th>
      <th>Empresa</th>
      <th>Salario</th>
      <th>Fecha</th>
    </tr>
    <tr>
      <th>Si</th>
      <td>`+ enterpriseSi + `</td>
      <td>`+ salarySi + `</td>
      <td>`+ dateSi + `</td>
    </tr>
    <tr>
      <th>No</th>
      <td>`+ enterpriseNo + `</td>
      <td>`+ salaryNo + `</td>
      <td>`+ dateNo + `</td>
    </tr>
    <tr>
      <th>Reportado</th>
      <td colspan="3">`+ reportado + `</td>
    </tr>      
  </table>
  `;

  return tablediv;
}

function tableInfoComputrabajo(enterprise, salary, rating, verified, date, serviceReport) {

  let icon = browser.runtime.getURL("icons/security-48.png");
  let enterpriseSi = ""; let enterpriseNo = "";
  let salarySi = ""; let salaryNo = "";
  let ratingSi = ""; let ratingNo = "";
  let verifiedSi = ""; let verifiedNo = "";
  let dateSi = ""; let dateNo = "";
  let reportado = "";
  switch (serviceReport) {
    case 0:
      reportado = "No reportado"
      break;
    case 1:
      reportado = serviceReport+" Vez reportado"
      break;       
    default:
      reportado = serviceReport+" Veces reportado"
      break;
  };

  if (enterprise !== 0)
    enterpriseSi = "x";
  else
    enterpriseNo = "x";

  if (salary !== 0)
    salarySi = "x";
  else
    salaryNo = "x";

  if (rating !== 0)
    ratingSi = "x";
  else
    ratingNo = "x";

  if (verified !== 0)
    verifiedSi = "x";
  else
    verifiedNo = "x";

  if (date !== 0)
    dateSi = "x";
  else
    dateNo = "x";

  let tablediv = `
  <table class="timecard">
    <tr>
    <th style="background-color:#fff"><img style="width:20px;" alt="" src="`+icon+`" /></th>
      <th>Empresa</th>
      <th>Salario</th>
      <th>Evaluación</th>
      <th>Verificado</th>
      <th>Fecha</th>
    </tr>
    <tr>
      <th>Si</th>
      <td>`+ enterpriseSi + `</td>
      <td>`+ salarySi + `</td>
      <td>`+ ratingSi + `</td>
      <td>`+ verifiedSi + `</td>
      <td>`+ dateSi + `</td>
    </tr>
    <tr>
      <th>No</th>
      <td>`+ enterpriseNo + `</td>
      <td>`+ salaryNo + `</td>
      <td>`+ ratingNo + `</td>
      <td>`+ verifiedNo + `</td>
      <td>`+ dateNo + `</td>
    </tr>
    <tr>
      <th>Reportado</th>
      <td colspan="5">`+ reportado + `</td>
    </tr>    
  </table>
  `;

  return tablediv;
}

/**
 Get color getBgColor
 */
function getBgColor(value) {
  let color = "";
  for (i = 0; i < RANGE_CP.length; i++) {
    if (RANGE_CP[i].rating === value)
      color = RANGE_CP[i].background;
  }
  return color;
}

/**
 Get rating from Multitrabajo
 */
async function Multitrabajo(idPubJob) {
  //document.body.style.border = "25px solid red";  
  const datePublication = null;
  let compareDatesMultitrabajo = 0;
  try {    
    let datePublication= await fetchData(idPubJob);    
    let dateArray = (datePublication.aviso.fechaPublicacion).split("-");
    let dateToValidated = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];    
    compareDatesMultitrabajo = compareDate(new Date(dateToValidated), get30Days(), new Date());
  } catch (error) {
    compareDatesMultitrabajo = 0;
  }



  /*Enterprise information extracted from classes "FichaAvisoSubHeader__Company-sc-1poii24-2"*/
  let enterpriseInformation = document.getElementsByClassName("FichaAvisoSubHeader__Company-sc-1poii24-2");

  /*information extracted from classes "DetalleAviso__InfoLabel-sc-1kfhr6j-1"*/
  let salaryInformation = document.getElementsByClassName("DetalleAviso__InfoLabel-sc-1kfhr6j-1");


  let enterpriseMultitrabajo = getenterpriseNameItemMulti(enterpriseInformation[0]);
  //console.log("enterpriseMultitrabajo", enterpriseMultitrabajo);

  let salaryMultitrabajo = getSalaryItemMulti(salaryInformation[3]);
  //console.log("salaryMultitrabajo", salaryMultitrabajo);



  let totalRatingMultitrabajo = enterpriseMultitrabajo + salaryMultitrabajo + compareDatesMultitrabajo;
  //console.log("totalRatingMultitrabajo",totalRatingMultitrabajo);

  let totalRatingMT = (5 * totalRatingMultitrabajo) / 3;
  //console.log("totalRatingMT",totalRatingMT);
  let ratingRangeMultitrabajo = getValueFromRangeMap(totalRatingMT, RANGE_CP);

  let arrayValues = {
    ratingRangeMultitrabajo: ratingRangeMultitrabajo,
    enterpriseMultitrabajo: enterpriseMultitrabajo,
    salaryMultitrabajo: salaryMultitrabajo,
    compareDatesMultitrabajo: compareDatesMultitrabajo
  };

  return arrayValues;

}



async function fetchData(idJob) {
  
  let hs = new Headers();
  hs.append("x-site-id","BMEC");
  hs.append("x-session-jwt",localStorage.getItem('sessionJwt'));
  let config = {
    method: "GET",
    headers: hs,    
  };
  const url = URL_MULTITRABAJO_API + idJob;
  const response = await fetch(url,config);
  const data = await response.json();
  return data; // It should already be parsed JSON since you called `response.json()`
}


/**
 * Get Name Field
 */
function getenterpriseNameItemMulti(tagUl) {
  if (tagUl.innerText.length === 0)
    return 0
  else
    if (tagUl.innerText.toUpperCase() === FIELDS.confidential)
      return 0;
    else
      return 1;
}

/**
 * Get Salary Field
 */
function getSalaryItemMulti(tagSalary) {
  /* Obtain Salary field */
  if (tagSalary.innerText.toUpperCase() === FIELDS.noSpecified)
    return 0;
  else
    return 1;
}

/**
  Get rating from Computrabajo
  */
function Computrabajo() {
  
  /**
     * Computrabajo
     * Complete Information to iterate (this information is extracted from webPage by ClassName)   
    */

  /*enterprise information extracted from classes "cm-12 box_resume_offer divClick desocu cp"*/
  let enterpriseInformation = document.getElementsByClassName("cm-12 box_resume_offer divClick desocu cp");

  /*information extracted from classes "box box_r"*/
  let listInformation = document.getElementsByClassName("box box_r");

  /*information extracted from classes "icon sello"*/
  let iconSello = document.getElementsByClassName("icon sello");

  /*information extracted from classes "cm-12 box_i bWord"*/
  let datePosted = document.getElementsByClassName("cm-12 box_i bWord");

  /*Sumary Information (review ,veriy position 0) akiver*/
  let sumaryInformation = listInformation[0].children;

  /*obtain the ul information*/
  let tagUl = null;
  for (i = 0; i < sumaryInformation.length; i++) {
    let isUl = getIsSumaryTagUl(sumaryInformation[i]);
    if (isUl) {
      tagUl = sumaryInformation[i];
      break;
    }
  };
  let enterpriseComputrabajo = getEnterpriseNameItem(tagUl);
  //console.log("enterpriseComputrabajo", enterpriseComputrabajo);

  let salaryComputrabajo = getSalaryItem(tagUl);
  //console.log("salaryComputrabajo", salaryComputrabajo);

  let ratingComputrabajo = getenterpriseRatingItem(enterpriseInformation);
  //console.log("ratingComputrabajo", ratingComputrabajo);
  let ratingCP = (RATING_3.MAX * ratingComputrabajo) / RATING_CP.MAX;
  //console.log('en regla 3' + ratingCP);
  //console.log("ratingComputrabajo", ratingComputrabajo + " 'en regla 3' " + ratingCP);


  let verifiedEnterprise = getSelloItem(iconSello);
  //console.log("verifiedEnterprise", verifiedEnterprise);

  let dateValidated = getDatePosted(datePosted);
  let compareDates = compareDate(dateValidated, get30Days(), new Date());
  //console.log("compareDates", compareDates);

  let totalRating = enterpriseComputrabajo + salaryComputrabajo + ratingCP + verifiedEnterprise + compareDates;
  //console.log("totalRating", totalRating);

  //console.log("totalRating", totalRating);
  let ratingRange = getValueFromRangeMap(totalRating, RANGE_CP);

  let arrayValues = {
    ratingComputrabajo: ratingRange,
    enterpriseComputrabajo: enterpriseComputrabajo,
    salaryComputrabajo: salaryComputrabajo,
    ratingCP: ratingCP,
    verifiedEnterprise: verifiedEnterprise,
    compareDates: compareDates
  };

  return arrayValues;
};

/**
 * Get compare Dates
 */
function compareDate(date, start, end) {
  if (date >= start && date <= end)
    return 1;
  else
    return 0;
};


/**
 * Get - 30 days
 */
function get30Days() {
  let thirtyDays = 1000 * 60 * 60 * 24 * 30;

  let today = new Date();
  let minus = today.getTime() - thirtyDays;
  let dateThirty = new Date(minus);

  return dateThirty;
};


/**
 * Get date posted
 */
function getDatePosted(item) {
  let dateReturn = null;
  for (i = 0; i < item[0].children.length; i++) {
    if (getIsTagMeta(item[0].children[i])) {
      if (item[0].children[i].attributes.itemprop.value.toUpperCase() == FIELDS.date) {
        dateReturn = item[0].children[i].content;
      }
    }
  }
  dateReturn = new Date(dateReturn);
  return dateReturn;
};

/**
 * Get Range Value
 */
function getValueFromRangeMap(value, rangeMapData) {
  let range = null
  rangeMapData.forEach(element => {
    if (value >= element.min && value <= element.max) {
      range = element.rating;
    }
  });

  return range;
};

/**
 * Get enterprise Validated Field
 */
function getSelloItem(iconSello) {
  /* Obtain icon sello field */
  if (iconSello.length > 0) {
    return 1;
  }
  else {
    return 0;
  }
};


/**
 * Get enterprise Value Field
 */
function getenterpriseRatingItem(enterpriseTag) {
  /* Obtain Salary field */
  if (enterpriseTag.length > 0) {    
    let detailenterprise = document.getElementsByClassName("cm-9 detalle_textoempresa");

    let numRating = getRatingEnterpriseValue(detailenterprise);

    if (!numRating)
      numRating = "0";

    numRating = numRating.replace(",", ".");
    return parseFloat(numRating);
  }
  else {
    return 0;
  }
};


/**
 * Get Enterprise Name Field
 */
function getEnterpriseNameItem(tagUl) {
  /* Obtain Salary field */
  let name = null;
  for (i = 0; i < tagUl.children.length; i++) {
    let isEnterprise = getIsNameenterpriseField(tagUl.children[i]);
    if (isEnterprise) {
      name = tagUl.children[i].children[1].innerText;
      break;
    }
  }
  if (name && name.length > 0)
    return 1;
  else
    return 0;
};


/**
 * Get Rating Value Enterprise
 */
function getRatingEnterpriseValue(detailenterprise) {
  /* Obtain value rating */

  let ratingValue = null;
  for (i = 0; i < detailenterprise[0].children.length; i++) {
    let isRatingVal = getIsRatingValue(detailenterprise[0].children[i]);
    if (isRatingVal) {

      ratingValue = detailenterprise[0].children[i].content;
      break;
    }
  };

  if (!ratingValue) {
    let view = null;
    let contentA = document.getElementsByClassName("cm-9 detalle_textoempresa w_auto");
    if(contentA.length>0){
      for ( n = 0; n < contentA[0].children.length; n++) {
        if (contentA[0].children[n].tagName.toUpperCase()==='A' && contentA[0].children[n].className==="empr it-blank" ){
          view=contentA[0].children[n];
          break;
        }      
      }
      if(view){
        let numero = parseFloat((view.children[0].children[0].children[1].style.width).replace("px", ""));
        ratingValue= getRatingStarts(numero, RATE_STARS)+"";
      }
      else{
        ratingValue=0+"";
      }
    }else{
      ratingValue=0+"";
    }
  }

  return ratingValue;
};

//when rating come in <a> only with starts
function getRatingStarts(value, rangeMapData) {

  if (!value || value <= 0) {
    return rangeMapData[0].rateTotal;
  }

  let range = null
  rangeMapData.forEach(element => {
    if (value >= element.min && value <= element.max) {
      range = element.rateTotal;
    }
  });

  return range;
};


/**
 * Get Salary Field
 */
function getSalaryItem(tagUl) {
  /* Obtain Salary field */
  let salaryValue = null;
  for (i = 0; i < tagUl.children.length; i++) {
    let isSalary = getIsSalaryField(tagUl.children[i]);
    if (isSalary) {
      salaryValue = tagUl.children[i].children[1].innerText;
      break;
    }
  }

  let withSalary = !isNaN(((salaryValue.split(" ")[0]).replace(".", "")).replace(",", ""));
  if (withSalary)
    return 1;
  else
    return 0;
};


/**
 * Item to verify if is a RatingValue
 */
function getIsRatingValue(item) {
  if(item.attributes.length>0){
    if (getIsTagMeta(item)) {
      if (item.attributes.itemprop.value.toUpperCase() === FIELDS.ratingValue) {
        return true;
      }
      else
        return false;
    }
    else return false;
  }
  else return false;  
};

/**
 * Item to verify if is tag meta
 */
function getIsTagMeta(item) {
  if (item.tagName.toUpperCase() === TAGS.meta)
    return true;
  else
    return false;
};

/**
 * Item to verify if is a Salary
 */
function getIsSalaryField(item) {
  if (item.children.length > 0)
    if (item.children[0].innerText.toUpperCase() === FIELDS.salary)
      return true;
    else
      return false;
  else
    return false;
};

function getIsNameenterpriseField(item) {
  if (item.children.length > 0)
    if (item.children[0].innerText.toUpperCase() === FIELDS.enterprise)
      return true;
    else
      return false;
  else
    return false;
};

/**
 * Item to verify if is a Ul tag
 */
function getIsSumaryTagUl(item) {
  if (item.tagName === TAGS.ul)
    return true;
  else
    return false;
};

async function fetchXmlMehod(url) {
  let hs = new Headers();
  hs.append("Authorization","Basic " + btoa("acclabecu:pas45frKLswd"));
  let config = {
    method: "GET",
    headers: hs,    
  };
  const response = await fetch("https://middle-kobo.cyberzen.ec/submissionscount?url=\""+url+"\"",config);
  const data = await response.json();
  return data; // It should already be parsed JSON since you called `response.json()`
};
