function myReport() {

    let uuiD = create_uuid();
    //alert(uuiD);
    let url_denuncia = document.getElementById("url_denuncia").value;
    let ul_opciones = document.getElementById("opciones");
    let categories = "";
    let isChecked = false;
    //console.log(ul_opciones.children.length);
    for (i = 0; i < ul_opciones.children.length; i++) {
        if (ul_opciones.children[i].children[0].checked) {
            categories = categories + ul_opciones.children[i].children[0].id + " ";
            ul_opciones.children[i].children[0].checked = false;
            // console.log(ul_opciones.children[i].children[0].checked);
            isChecked = true;
        }
    };
    //console.log(categories);
    if (!isChecked) {
        alert('Por favor, selecciona al menos una opción!');
        return false;
    }
    else {

        if (verificarUrl(url_denuncia)) {
            categories = categories.trim();
            //console.log("categories", categories, url_denuncia);


            var request = new XMLHttpRequest();

            request.open('POST', 'https://middle-kobo.cyberzen.ec/submissions', true);
            request.setRequestHeader("Authorization", "Basic " + btoa("acclabecu:pas45frKLswd"));

            request.onreadystatechange = function () {
                if (this.readyState === 4) {
                    console.log('Status:', this.status);
                    console.log('Headers:', this.getAllResponseHeaders());
                    console.log('Body:', this.responseText);
                }
            };

            /*get date report*/
            let date = new Date();

            var body = "<data id=\"aqQcqsJeCYKumfBv4WV6qN\" >" +
                "    <device_id>kobo-middleware</device_id>" +
                "    <start type='str'>" + date.toLocaleString() + "</start>" +
                "    <end type='str'>" + date.toLocaleString() + "</end>" +
                "    <url type='str'>" + url_denuncia + "</url>" +
                "    <categoria type='str'>" + categories + "</categoria> " +
                "    <_uuid type=\"str\">" + uuiD + "</_uuid>" +
                "    <meta>" +
                "        <instanceID>aqQcqsJeCYKumfBv4WV6qN</instanceID>" +
                "    </meta>" +
                " </data>";

            var formData = new FormData()
            var blob = new Blob([body], { type: 'text/xml' })
            formData.append('file', blob)
            request.send(formData);
            return true;
        } else {
            alert('Por favor, ingrese una URL valida!');
            return false;
        }
    }
};

/*Create uuid for webservice*/
function create_uuid() {
    let dt = new Date().getTime();
    //let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[x,y]/g, function (c) {
    let uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[x,y]/g, function (c) {
        let r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });

    return uuid;
};

function verificarUrl(url) {
    var parser = document.createElement('a');
    try {
        parser.href = url;
        parser.protocol; // => "http:"
        parser.hostname; // => "example.com"
        parser.port; // => "3000"
        parser.pathname; // => "/pathname/"
        parser.search; // => "?search=test"
        parser.hash; // => "#hash"
        parser.host; // => "example.com:3000"

        if (parser.host === "www.computrabajo.com.ec" || parser.host === "www.multitrabajos.com") {
            if (parser.pathname.includes("/empleos/") || parser.pathname.includes("/ofertas-de-trabajo/")) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } catch (error) {
        return false;
    };
}

/*Report employ advertisement*/
window.onload = function () {
    document.getElementById('denunciar').addEventListener('click', function (e) {
        e.preventDefault();
        if (myReport()) {
            var form = document.getElementById("form");
            form.reset();
        }
    });
    URLDinamyc();
};


function URLDinamyc() {
    //let url = null;
    browser.tabs.query({ currentWindow: true, active: true }, function (tabs) {
        url = tabs[0].url;
        if (url)
            if (verificarUrl(url)) {
                document.getElementById("url_denuncia").value = url;
            } else {
                document.getElementById("url_denuncia").value = "";
            }
    });

}
