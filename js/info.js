/*Hamburguer Menu*/
document.addEventListener("click", (e) => {
        
  if (e.target.id === "hamburger") {
   openRecommendation("opciones.html");
  }
    e.preventDefault();
});

/*Show Recommendation*/
function openRecommendation (url) {
  var form = document.createElement('form')
  form.method = 'POST'
  form.action = url;
  document.body.appendChild(form)
  form.submit()
}


/*Removes diacritics from text except if it is "ñ" (ES6)*/
function removeDiacritical (text) {
    return text
    .normalize('NFD')
    .replace(/([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi,"$1")
    .normalize();
}


/*Search job only computrabajo or multritrabajo*/
document.addEventListener("click", (e) => {

  var site = 'https://'                   
  var newString = removeDiacritical (document.getElementById("searchtext").value).trim().replace(/\s/g, '-')
  if (e.target.id === "tabs-computrabajo") {
      site +='www.computrabajo.com.ec/trabajo-de-' + newString + '?q=' + newString
   }
  else{
    if (e.target.id === "tabs-multitrabajo") {
      site +='www.multitrabajos.com/empleos-busqueda-' + newString + '.html'
    }
  }
    browser.tabs.create({url:site});
    e.preventDefault();

});


/*Create elements for list */
function createElements(url,icon,text){

  let input = document.createElement("a");
  input.setAttribute("id", "acess");
  input.setAttribute("href","javascript:void(0)");
  input.setAttribute("class","link-secondary bold");

  input.addEventListener('click',function(){
    openRecommendation(url);
  })

  let span = document.createElement("span");
  span.setAttribute("class",icon);

  let arrow = document.createElement("span");
  arrow.setAttribute("class","icon icon__arrow pull-right"); 

  let textAccess = document.createElement("span");
  textAccess.setAttribute("class","is-active text-line-after-icon");
  textAccess.textContent = text;

  input.appendChild(span);
  input.appendChild(arrow);
  input.appendChild(textAccess);

  return input;
}


/**
 * create icons to recommendation of security
 */
function createIcons() {

    var ul = document.getElementsByTagName("ul")[0]; 
    var docfrag = document.createDocumentFragment();
    var browserList = ["Navegacion", "Privacidad", "Antivirus","Comunicaciones","Denuncias","Imagen"];

    browserList.forEach(function(e) {
      var li = document.createElement("li");
      li.setAttribute("class","padded border--bottom");

    switch (e) {
      case "Navegacion":
          li.appendChild(createElements('navegacion.html','site-info__spyware__icon','Navegación Segura'));
          break;
      case "Privacidad":
          li.appendChild(createElements('privacidad.html','site-info__access__icon','Privacidad'));
          break;
      case "Antivirus":
          li.appendChild(createElements('antivirus.html','site-info__antivirus__icon','Antivirus y Actualizaciones'));
          break;
      case "Comunicaciones":
          li.appendChild(createElements('comunicaciones.html','site-info__communication__icon','Comunicaciones Seguras'));
          break;
      case "Denuncias":
            li.setAttribute("class","site-info__li--toggle padded is-active");
            li.appendChild(createElements('reportar.html','site-info__denuncia__icon','Reportar Anuncio'));
          break;
      case "Imagen":
            let span = document.createElement("span");
            span.setAttribute("class","site-info__footer__icon");
            li.appendChild(span);  
          break;
      default:
          break;
    }

    docfrag.appendChild(li);
    });

    ul.appendChild(docfrag);
}

document.addEventListener("DOMContentLoaded", createIcons);

