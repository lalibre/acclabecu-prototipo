document.body.onload = drawElement;

// Create div for body
function drawElement () {
// Create div for search
  var search = _taggedTemplateLiteral(['<section>\n <form id="formSearch" onsubmit="return false;" name="formSearch" class="sliding-subview__header search-form">\n  <span style="display:inline-block;">\n <img class="manImg" src="../icons/security-48.png"></img> \n  <input id="searchtext" type="text" autocomplete="off" placeholder="Buscar un empleo en:" name="q" size="50" class="search-form__input" required>\n<button id="hamburger" type="button" class="hamburger-button"><span></span><span></span><span></span></button> </span>\n <div class="search-options"><span style="align-content: center"><a href="#" id="tabs-multitrabajo" required>Multitrabajo</a>\n o en \n<a href="#" id="tabs-computrabajo" required>Computrabajo</a> <br>\n</span>\n </div>\n </form>\n </section>']);
  var options = _taggedTemplateLiteral(['<section class="site-info se-info--main">\n  <ul class="default-list">\n </ul>\n  </section>']);
  templateElement(search, 'popup-container');
  templateElement(options, 'popup-detail');
}

function _taggedTemplateLiteral (strings, raw) {
  return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } }))
}

function templateElement (object, idObject) {
  function htmlToElement (object) {
    var element = document.createElement('div');
    element.innerHTML = object;
    return (element);
  }

  var searchConcat = htmlToElement(object);
  document.getElementById(idObject).appendChild(searchConcat);
}

