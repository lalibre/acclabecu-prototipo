# Prototipo extensión: Seguridad Digital y búsqueda de empleo
---

Esta aplicación es desarrollada por [LaLibre.net Tecnologías Comunitarias](https://lalibre.net) para el [Laboratorio de Acceleración del PNUD](https://acclabecu.org)

## Funcionalidades:

- Búsqueda de empleo en varios sitios

- Análisis y puntuación básica de anuncios de empleo

- navegación en documentación en seguridad digital.

## Instalación:

Revisa el proceso de instalación en la sección: [Instalación complemento](https://gitlab.com/lalibre/acclabecu-prototipo/-/wikis/Instalaci%C3%B3n-del-complemento).